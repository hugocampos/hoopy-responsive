require 'test_helper'

class BetatestersControllerTest < ActionController::TestCase
  setup do
    @betatester = betatesters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:betatesters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create betatester" do
    assert_difference('Betatester.count') do
      post :create, :betatester => @betatester.attributes
    end

    assert_redirected_to betatester_path(assigns(:betatester))
  end

  test "should show betatester" do
    get :show, :id => @betatester
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @betatester
    assert_response :success
  end

  test "should update betatester" do
    put :update, :id => @betatester, :betatester => @betatester.attributes
    assert_redirected_to betatester_path(assigns(:betatester))
  end

  test "should destroy betatester" do
    assert_difference('Betatester.count', -1) do
      delete :destroy, :id => @betatester
    end

    assert_redirected_to betatesters_path
  end
end
