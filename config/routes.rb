Hoopy::Application.routes.draw do

  # landing fuera de "locale" porque cuando redirecciona a /en/xxx se pierden los estilos
  # pending: Arreglar los links de CSS en el Landing
  get 'landing' => 'betatesters#new'
  get 'landing_ok' => 'betatesters#ok'

  #get "admin/index"
  get 'admin' => 'admin#index'
  controller :sessions do
        get  'login' => :new
        post 'login' => :create
        #post 'selectfriend' => :setfriend
        delete 'logout' => :destroy   # HM: Hoopy Login (Pend: juntar con Omniauth)
  end

  scope '(:locale)' do
   match "/signout" => "sessions#destroy", :as => :signout
   get 'welcome' => 'users#welcome'  # HM: Revisar!!
   get 'store' => 'store#index'
   get 'ready' => 'orders#ready'
   get 'selectfriend' => 'orders#selectfriend'
   post 'selectfriend' => 'sessions#setfriend' # Guarda el "friend" en el hash de la sesión
   get 'shops' => 'store#shops'
   get 'allies' => 'allies#index'
   get 'tos' => 'static#tos'
   get 'privacy' => 'static#privacy'
   get 'contact' => 'static#contact'
   get 'about' => 'static#about'
   post 'deliver' => 'orders#confirmdelivery'
   resources :enterprises do
        resources :products
   end
   resources :orders do
     member do
        get 'userpreview'
        get 'dealerpreview'
        #get 'dealerconfirm'
     end
   end
   resources :users
   resources :line_items
   resources :carts
   resources :products
   resources :betatesters
   get 'landing' => 'betatesters#new'
   get 'landing_ok' => 'betatesters#ok'
   post 'contactform' => 'betatesters#contactform'
   #root :to => 'store#welcome', :as => 'index'
   root :to => 'betatesters#new', :as => 'landing'
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'

  #root :to => 'store#index', :as => 'store'

 # HM: Para omniauth:
 match "/auth/:provider/callback" => "sessions#create_omniauth"
 #match "/auth/failure" => "sessions#failure"
 #match "/auth/twitter/callback" => "sessions#create_omniauth"
 ## Puesto dentro del Scope i18n
 ##match "/signout" => "sessions#destroy", :as => :signout
 
end
