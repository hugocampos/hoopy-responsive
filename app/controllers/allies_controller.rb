class AlliesController < ApplicationController
 #skip_before_filter :authorize

  def index
        @enterprises = Enterprise.all
  end

  def show
	@enterprise = Enterprise.find(params[:id])
  end

end
