class StaticController < ApplicationController
  caches_page :tos, :privacy, :about, :contact
  skip_before_filter :authorize

  def tos
  end

  def privacy
  end

  def about
  end

  def contact
  end

  def landing
  end
end
