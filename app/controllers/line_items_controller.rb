class LineItemsController < ApplicationController
 skip_before_filter :authorize, :only => :create

  # GET /line_items
  # GET /line_items.json
  def index
    @line_items = LineItem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @line_items }
    end
  end

  # GET /line_items/1
  # GET /line_items/1.json
  def show
    @line_item = LineItem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @line_item }
    end
  end

  # GET /line_items/new
  # GET /line_items/new.json
  def new
    @line_item = LineItem.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @line_item }
    end
  end

  # GET /line_items/1/edit
  def edit
    @line_item = LineItem.find(params[:id])
  end

  # POST /line_items
  # POST /line_items.json
  def create
    @cart = current_cart
    product = Product.find(params[:product_id])
    #@line_item = LineItem.new(params[:line_item])
    #@line_item = @cart.line_items.build(:product => product)
    # si el cart no está vacío validar misma ENT
    if @cart.ensure_same_enterprise(product.id) == "ok"
     @line_item = @cart.add_product(product.id)
    else
     redirect_to store_path, :notice => I18n.t('.diffshops')
     return
    end

    respond_to do |format|
      if @line_item.save
        #format.html { redirect_to @line_item, :notice => 'Line item was successfully created.' }
        #format.html { redirect_to(@line_item.cart) }
        #format.html { redirect_to(store_path) }
	##format.html { redirect_to(product.enterprise) }  # Redirige a la misma Tienda del producto seleccionado! :-)
	format.html { redirect_to(new_order_path) }  # Sin Shopping Cart! Solo para el MVP del Internal DemoDay! 
	format.js
        format.json { render :json => @line_item, :status => :created, :location => @line_item }
      else
        format.html { render :action => "new" }
        format.json { render :json => @line_item.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /line_items/1
  # PUT /line_items/1.json
  def update
    @line_item = LineItem.find(params[:id])

    respond_to do |format|
      if @line_item.update_attributes(params[:line_item])
        format.html { redirect_to @line_item, :notice => 'Line item was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @line_item.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /line_items/1
  # DELETE /line_items/1.json
  def destroy
    @line_item = LineItem.find(params[:id])
    @line_item.destroy

    respond_to do |format|
      format.html { redirect_to line_items_url }
      format.json { head :ok }
    end
  end
end
