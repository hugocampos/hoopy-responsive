class OrdersController < ApplicationController
 #TODO: Restringir acceso (Si se habilita la linea de abajo)
 #skip_before_filter :authorize

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
    #@user = Orders.user rescue User.find(@orders.user_id)
    #@micropost = Micropost.find(params[:id])
    # Hack to handle Rails bug:
    #@user = Micropost.user rescue User.find(@micropost.user_id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @orders }
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    #@order = Order.find(params[:id])
    @order = Order.find_by_hashed_id(params[:id])
    @user = Order.user rescue User.find(@order.user_id)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @order }
    end
  end

  # GET /orders/new
  # GET /orders/new.json
  def new
    # session:[friend] está en SessionsController 
    @sessionfriend = session[:friend]
    @sessionfriend_fb_name = session[:friend_fb_name]
    @sessionfriend_fb_num = session[:friend_fb_num]
    @cart = current_cart
    if @cart.line_items.empty?
	redirect_to allies_path, :notice => "Your cart is empty"
	return
    end

    @order = Order.new

    respond_to do |format|
      format.html # new.html.erb
      format.js
      format.json { render :json => @order }
    end
  end

  # GET /orders/1/edit
  def edit
    #@order = Order.find(params[:id])
    @order = Order.find_by_hashed_id(params[:id])
  end

  # POST /orders
  # POST /orders.json
  def create
    #@order = Order.new(params[:order])
    @order = current_user.orders.build(params[:order])
    @order.set_enterprise_owner(current_cart)
    @order.add_line_items_from_cart(current_cart)

    respond_to do |format|
      if @order.save
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
	## HM: Deshabilitado envío de correos para que funcione en Heroku!!
	## HM: Notifier.order_received(@order).deliver
        #format.html { redirect_to store_index_path, :notice => 'Orden creada. Gracias por usar Hoopy.' }
        format.html { redirect_to ready_path, :notice => I18n.t('.thanks') }
	#format.js
        format.json { render :json => @order, :status => :created, :location => @order }
      else
        format.html { render :action => "new" }
        format.json { render :json => @order.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /orders/1
  # PUT /orders/1.json
  def update
    #@order = Order.find(params[:id])
    @order = Order.find_by_hashed_id(params[:id])

    # Confirm the code matches:
    if (@order.codrecgift != params[:order][:codrecgift].to_i)
       flash[:alert] = "#{params[:order][:codrecgift]} #{t('msgs.confirmation_mismatch')}" 
       redirect_to welcome_path and return
    end

    respond_to do |format|
      if @order.update_attributes(params[:order])
        #format.html { redirect_to @order, :notice => 'Order was successfully updated.' }
        format.html { redirect_to dealerpreview_order_path(@order), :notice => "#{t('msgs.order_updated')}" }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @order.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    #@order = Order.find(params[:id])
    @order = Order.find_by_hashed_id(params[:id])
    @order.destroy

    respond_to do |format|
      format.html { redirect_to orders_url }
      format.json { head :ok }
    end
  end

  def userpreview 
    #@order = Order.find(params[:id])
    @order = Order.find_by_hashed_id(params[:id]) # Other TESTING: .where("beneficiary = ? OR beneficiary_uid = ?", current_user.nickname, current_user.uid)
    @user = Order.user rescue User.find(@order.user_id)
    #@url = "https://huupy.com/orders/#{@order.hashed_id}/dealerpreview"
    @url = "https://nameless-headland-1824.herokuapp.com/orders/#{@order.hashed_id}/dealerpreview?c=#{@order.codrecgift}"

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @order }
    end
  end

  def dealerpreview
    #@order = Order.find(params[:id])
    @order = Order.find_by_hashed_id(params[:id])
    @url = "https://huupy.com/orders/#{@order.hashed_id}/dealerpreview"

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @order }
    end
  end
 
  def dealerconfirm
    #@order = Order.find(params[:id])
    @order = Order.find_by_hashed_id(params[:id])
  end
 
  def confirmdelivery   # ????? 
    @order = Order.find_by_hashed_id(params[:hashed_id])
    respond_to do |format|
        if (@order.codrecgift.to_s() == params[:codrecgift])
                @order.update_attributes(:okshop_at => Time.now)
	  	#render :text => @order.hashed_id #params #params[:codrecgift] 
		format.html { redirect_to dealerpreview_order_path(@order), :notice => "#{t('msgs.order_updated')}" }
		format.json { head :ok }
	else
		#@order.codrecgift #{params[:codrecgift]}" #"No actualizado"
		format.html { redirect_to dealerpreview_order_path(@order), :notice => "#{t('msgs.confirmation_mismatch')}" }
		format.json { render :json => @order.errors, :status => :unprocessable_entity }
        end
    end
  end

  #def showavatar
  #  @avatar = "https://si0.twimg.com/profile_images/1239447061/Unnamed-1_reasonably_small.jpg"
  #end

  def ready
  end

  def selectfriend
        #if session[:friend].nil?
        #  @friend = ""
        #else
          #@friend = session[:friend]
          @friend ||= session[:friend] if session[:friend]
        #end
  end

  # def setfriend   <-- It's in SessionsController

end
