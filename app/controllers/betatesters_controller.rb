class BetatestersController < ApplicationController
  skip_before_filter :authorize, :only => [:new, :create, :contactform, :ok]

  # GET /betatesters
  # GET /betatesters.json
  def index
    @betatesters = Betatester.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @betatesters }
    end
  end

  # GET /betatesters/1
  # GET /betatesters/1.json
  def show
    @betatester = Betatester.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @betatester }
    end
  end

  # GET /betatesters/new
  # GET /betatesters/new.json
  def new
    @betatester = Betatester.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @betatester }
    end
  end

  # GET /betatesters/1/edit
  def edit
    @betatester = Betatester.find(params[:id])
  end

  # POST /betatesters
  # POST /betatesters.json
  def create
    @betatester = Betatester.new(params[:betatester])

    respond_to do |format|
      if @betatester.save
        #format.html { redirect_to @betatester, :notice => 'Listo! Te tenemos registrado' }
        #format.html { redirect_to landing_path, :notice => 'Listo! Te avisaremos en cuanto lancemos el beta!' }
        format.html { redirect_to landing_ok_path }
        format.json { render :json => @betatester, :status => :created, :location => @betatester }
      else
        #format.html { render :action => "new" }
        format.html { redirect_to landing_path, :alert => I18n.t('.ups_try_again') }
        format.json { render :json => @betatester.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /betatesters/1
  # PUT /betatesters/1.json
  def update
    @betatester = Betatester.find(params[:id])

    respond_to do |format|
      if @betatester.update_attributes(params[:betatester])
        format.html { redirect_to @betatester, :notice => 'Betatester was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @betatester.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /betatesters/1
  # DELETE /betatesters/1.json
  def destroy
    @betatester = Betatester.find(params[:id])
    @betatester.destroy

    respond_to do |format|
      format.html { redirect_to betatesters_url }
      format.json { head :no_content }
    end
  end

  # Formulario de contacto del Landing Page
  def contactform
    #@datosform = params[:q]
    #@nombre = params[:f_nombre][:f_telefono][:f_email][:f_compania][:f_mensaje]
    @nombre = params[:f_nombre]
    #@telefono = params[:f_telefono]
    @email = params[:f_email]
    #@compania = params[:f_compania]
    @mensaje = params[:f_mensaje]
    Betatester.manipuladatos(@nombre, @email, @mensaje)
    respond_to do |format|
      #format.html { redirect_to landing_url, :notice => 'Gracias. Tus datos fueron enviados correctamente.' }
      format.html { redirect_to landing_ok_url }
      format.json { head :no_content }
    end
  end

  def ok
  end

end
