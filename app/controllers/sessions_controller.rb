class SessionsController < ApplicationController
 skip_before_filter :authorize
  def new
  end

  def create
	if user = User.authenticate(params[:nickname], params[:password])
		session[:user_id] = user.id
		redirect_to welcome_path
	else
		redirect_to login_url, :alert => "Invalid user/password combination"
	end
  end

  def setfriend
        session[:friend] = params[:nickname]
	session[:friend_fb_name] = params[:fbfriend_name]
	session[:friend_fb_num] = params[:fbfriend_num]
        redirect_to allies_path
  end

  ##### Para OMNIAUTH #####
  def create_omniauth
        #raise request.env["omniauth.auth"].to_yaml
        auth = request.env["omniauth.auth"]
  	user = User.find_by_provider_and_uid(auth["provider"], auth["uid"]) || User.create_with_omniauth(auth)
  	session[:user_id] = user.id
  	#redirect_to store_url, :notice => "Signed in!"
  	#redirect_to welcome_url, :notice => "Signed in!"  # "Your Gifts
  	redirect_to selectfriend_path   # , :notice => "Signed in!"  # "Your Gifts
  end
  def failure
	# HM: Revisar que se muestre el Mensaje
	#redirect_to store_index_path, :flash => {:error => "Could not log you in. #{params[:message]}"}
	#redirect_to index_path, :notice => "Could not log you in. #{params[:message]}"
	redirect_to landing_path, :notice => "Could not log you in. #{params[:message]}"
  end
  #####################

  def destroy
	#session[:user_id] = nil
	reset_session   # Clears the whole session[...etc..] hash! :-)
	current_cart.destroy
	#redirect_to store_url, :notice => "Logged out"
	#redirect_to index_path, :notice => "Logged out"
	redirect_to landing_path, :notice => "Logged out"
  end

end
