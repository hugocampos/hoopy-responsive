class UsersController < ApplicationController
  # GET /users
  # GET /users.json
  def index
    @users = User.order(:nickname)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @users }
    end
  end

  def welcome
    #@user = User.find(params[:id])

    #### Gifts to redeem
    # Gifts to redeem TWITTER: (nickname, provider)
    @orders = Order.where(:beneficiary => current_user.nickname, :provider_type => current_user.provider, :okshop_at => nil).paginate :page => params[:page], :order=> 'created_at DESC', :per_page => 20
    # Gifts to redeem FACEBOOK: (uid, provider)
    # @orders = Order.where(:beneficiary_uid => current_user.uid, :provider_type => current_user.provider, :okshop_at => nil).paginate :page => params[:page], :order=> 'created_at DESC', :per_page => 20

    @orders_sent = Order.where(:user_id => current_user.id).paginate :page => params[:page], :order=> 'created_at DESC', :per_page => 20

    #### Redeemed gifts
    # Redeemed gifts TWITTER:
    @orders_picked = Order.where(:beneficiary => current_user.nickname, :provider_type => current_user.provider).paginate :page => params[:page], :order=> 'created_at DESC', :per_page => 20
    # Redeemed gifts FACEBOOK:
    # @orders_picked = Order.where(:beneficiary_uid => current_user.uid, :provider_type => current_user.provider, :okshop_at => !nil).paginate :page => params[:page], :order=> 'created_at DESC', :per_page => 20

    ### All gifts
    # TWITTER:
    @orders_all = Order.where("(beneficiary = ? AND provider_type = ?) OR (user_id = ?)", current_user.nickname, current_user.provider, current_user.id).paginate :page => params[:page], :order=> 'created_at DESC', :per_page => 20

    @total_orders = Order.count 
    @shop_orders_pending = Order.where(:enterprise_id => current_user.enterprise_id, :okshop_at => nil)
    @shop_orders_delivered = Order.where(:enterprise_id => current_user.enterprise_id)
    respond_to do |format|
      format.html # welcome.html.erb
      format.json { render :json => @user }
    end
  end
  
  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        #format.html { redirect_to @user, :notice => 'User #{@user.name} was successfully created.' }
        format.html { redirect_to(users_url, :notice => "User #{@user.nickname} was successfully created.") }
        format.json { render :json => @user, :status => :created, :location => @user }
      else
        format.html { render :action => "new" }
        format.json { render :json => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to(users_url, :notice => "User #{@user.nickname} was successfully updated.") }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    begin
     @user.destroy
     flash[:notice] = "User #{@user.nickname} deleted"
    rescue Exception => e
     flash[:notice] = e.message
    end

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :ok }
    end
  end
end
