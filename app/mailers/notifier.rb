class Notifier < ActionMailer::Base
  default :from => "Huupy Social Gifting <no-reply@huupy.com>"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.order_received.subject
  #
  #def order_received
  def order_received(order)
    @order = order
    #@url = "http://huupy.com/orders/#{@order.hashed_id}/dealerpreview"
    @url = "http://huupy.com/orders/#{@order.hashed_id}/userpreview"
    @google_qr = "https://chart.googleapis.com/chart?chs=300x300\&cht=qr\&chl=#{@url}\&choe=UTF-8"
    #attachments.inline['Voucher.png'] = File.read('/xxx/file.jpg')

    mail :to => order.email, :subject => "Tienes un nuevo Huupy Gift"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.order_shipped.subject
  #
  #def order_shipped
  def order_shipped(order)
    @order = order

    mail :to => order.email, :subject => "Huupy Gift entregado"
  end
end
