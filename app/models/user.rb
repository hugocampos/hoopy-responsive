require 'digest/sha2'

class User < ActiveRecord::Base
 has_many :orders
 belongs_to :enterprise
 #validates :name, :presence => true, :uniqueness => true

 validates :password, :confirmation => true
 attr_accessor :password_confirmation
 attr_reader :password

 #validate :password_must_be_present

 #after_create :create_hashed_id
 after_destroy :ensure_an_admin_remains
 #after_initialize :init
 #
 #   def init
 #     self.image_url  ||= 0.0           #will set the default value only if it's nil
 #     self.provider  ||= 0.0           #will set the default value only if it's nil
 #     self.address ||= build_address #let's you set a default association
 #   end
 before_save :default_values
 def default_values
   # Set default provider as "hoopy" to differentiate from FB & TW.
   self.provider ||= 'hoopy'
 end

##### Para OMNIAUTH #####
def self.create_with_omniauth(auth)
  create! do |user|
    user.provider = auth["provider"]
    user.uid = auth["uid"]
    #user.name = auth["info"]["nickname"]
    user.nickname = auth["info"]["nickname"]
    user.name = auth["info"]["name"]
    user.email = auth["info"]["email"]
    user.image_url = auth["info"]["image"]
    user.location = auth["info"]["location"]
    user.description = auth["info"]["description"]
    user.fbtoken = auth["credentials"]["token"]
  end
end
#####################

 def ensure_an_admin_remains
	if User.count.zero?
	  raise "Can't delete last user"
	end
 end

 def User.authenticate(nickname, password)
        #if user = find_by_name(name) 
        if user = find_by_nickname_and_provider(nickname, "hoopy") 
           if user.hashed_password == encrypt_password(password, user.salt)
                user
           end
        end
 end

 def User.encrypt_password(password, salt)
   Digest::SHA2.hexdigest(password + "wibble" + salt)
 end

# 'password' is a virtual attribute
 def password=(password)
        @password = password

        if password.present?
           generate_salt
           self.hashed_password = self.class.encrypt_password(password, salt)
        end
 end


private

  def password_must_be_present
   errors.add(:password, "Missing password") unless hashed_password.present?
  end

  def generate_salt
   self.salt = self.object_id.to_s + rand.to_s
  end

 #private
 # def self.hash_password(password)
 #	Digest::SHA1.hexdigest(password)
 # end

end
