class Cart < ActiveRecord::Base
	has_many :line_items, :dependent => :destroy

 def add_product(product_id)
        # Salir si existe un producto de otra ENTERPRISE
        #if line_items.first.product.enterprise_id? != product_id.enterprise_id
 	#   raise "Please do not mix products from different Shops in your Order"
        #end
	current_item = line_items.find_by_product_id(product_id)
	#OK: #{current_item.product.enterprise_id}
    	#OK: #{line_items.first.product.enterprise_id}
	if current_item
	  current_item.quantity += 1
	else
	  # aqui agregar si la ENT es igual ...
	  #if line_items.nil?
	#	current_item = line_items.build(:product_id => product_id)
	#  elsif current_item.product.enterprise_id == line_items.first.product.enterprise_id
	  	current_item = line_items.build(:product_id => product_id)
	#  else
		# Exception: raise "Please do not mix products from different Shops in your Order"
	end
	current_item
 end

 def total_price
   line_items.to_a.sum { |item| item.total_price }
 end
 
 def ensure_same_enterprise(product_id)
   current_product = Product.find(product_id)
   if line_items.empty? 
	return "ok"
   elsif current_product.enterprise_id == line_items.first.product.enterprise_id   
	return "ok"
   else
   end
 end


#### DRAFT TESTing #######
 #def ensure_same_enterprise
 #       if Cart.line_items.count.zero?
 #         # continue
 #       else
 #         raise "Can't delete last user"
 #       end
 #end
  # if current_item.product.enterprise_id !=  line_items.first.product.enterprise_id raise "No!"
##########################
end
