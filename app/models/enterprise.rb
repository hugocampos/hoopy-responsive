class Enterprise < ActiveRecord::Base
 validates :name, :logo_url, :presence => true
 validates :name, :uniqueness => true
 validates :logo_url, :format => {
  :with => %r{\.(gif|jpg|png)$}i,
  :message => 'must be a URL for GIF, JPG or PNG image'
 }

 has_many :products
 has_many :orders
 has_many :users

end
