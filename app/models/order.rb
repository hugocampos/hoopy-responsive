class Order < ActiveRecord::Base
  has_many :line_items, :dependent => :destroy
  belongs_to :enterprise
  belongs_to :user

   PAYMENT_TYPES = [ "Credit card", "Gift Card", "SafetyPay", "Tarjeta pre-pago", "Hoopy Points"]
   #PROVIDER_TYPES = [ "hoopy", "twitter", "facebook" ]
   PROVIDER_TYPES = [ "hoopy", "twitter", "facebook" ]

   #validates :name, :faceadd, :msg, :email, :pay_type, :presence => true
   validates :beneficiary, :pay_type, :presence => true # :msg, :email,   <- Making the Email notification optional for now ;-)
   validates :pay_type, :inclusion => PAYMENT_TYPES
   validates :provider_type, :inclusion => PROVIDER_TYPES

   after_create :create_hashed_id

   def to_param
     self.hashed_id
   end

   def add_line_items_from_cart(cart)
     cart.line_items.each do |item|
	item.cart_id = nil
	line_items << item
     end
   end

   def set_enterprise_owner(cart)
     self.update_attributes(:enterprise_id => cart.line_items.first.product.enterprise_id)
        #Cart.last.line_items.first.product.enterprise_id
   end

private
   def create_hashed_id
     self.update_attributes(:hashed_id => Digest::SHA2.hexdigest([Time.now, rand].join)[0,64])
   end

end
