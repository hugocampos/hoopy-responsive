class AddProviderTypeToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :provider_type, :string
    remove_column :orders, :faceadd, :string
  end
end
