class AddHashedIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :hashed_id, :string
  end
  #add_index :orders, :hashed_id
end
