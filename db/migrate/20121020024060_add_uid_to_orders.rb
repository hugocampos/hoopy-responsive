class AddUidToOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :name, :string
    remove_column :orders, :uid, :integer
    add_column :orders, :beneficiary, :string
    add_column :orders, :beneficiary_uid, :integer
  end
end
