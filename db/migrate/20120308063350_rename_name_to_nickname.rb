class RenameNameToNickname < ActiveRecord::Migration
  def up
    rename_column :users, :name, :nickname
    add_column :users, :name, :string
  end

  def down
  end
end
