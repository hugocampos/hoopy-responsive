class CreateBetatesters < ActiveRecord::Migration
  def change
    create_table :betatesters do |t|
      t.string :email

      t.timestamps
    end
  end
end
