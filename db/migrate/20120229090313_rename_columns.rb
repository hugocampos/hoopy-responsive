class RenameColumns < ActiveRecord::Migration
  def up
	remove_column :orders, :okshop
	remove_column :orders, :okuser
	add_column :orders, :okshop_at, :date
	add_column :orders, :okuser_at, :date
  end

  def down
  end
end
