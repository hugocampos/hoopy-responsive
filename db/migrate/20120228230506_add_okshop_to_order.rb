class AddOkshopToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :codrecgift, :integer
    add_column :orders, :enterprise_id, :integer
    add_column :orders, :okshop, :integer
    add_column :orders, :okuser, :integer
    add_column :orders, :recibido, :integer
    add_column :users, :email, :string
    add_index :orders, :user_id
    add_index :orders, :enterprise_id
    add_index :orders, :created_at
  end
end
