class RemoveImageUrlUsers < ActiveRecord::Migration
  def up
    remove_column :users, :image_url, :string
    add_column :users, :image_url, :string, :default => '/images/hoopypet-1color.png'
  end

  def down
  end
end
