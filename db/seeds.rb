# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Emanuel', :city => cities.first)
#---



# Excerpted from "Agile Web Development with Rails",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material, 
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose. 
# Visit http://www.pragmaticprogrammer.com/titles/rails4 for more book information.
#---
#---
# Excerpted from "Agile Web Development with Rails, 4rd Ed.",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material, 
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose. 
# Visit http://www.pragmaticprogrammer.com/titles/rails4 for more book information.
#---
# encoding: utf-8
Enterprise.delete_all
Enterprise.create(:name => 'SAGA falabella',
:logo_url => '/images/logo_saga_Falabella.jpg'
)
Enterprise.create(:name => 'Kentucky Fried Chicken',
:logo_url => '/images/logo_KFC.gif'
)
Enterprise.create(:name => 'T.G.I. Fridays',
:logo_url => '/images/logo_TGI_Fridays.gif'
)
Enterprise.create(:name => 'PRIMAX',
:logo_url => '/images/logo_primax-1.gif'
)
Enterprise.create(:name => 'Starbucks Coffee',
:logo_url => '/images/logo_Starbucks_Coffee.gif'
)
#Enterprise.create(:name => 'Taxi Seguro',
#:logo_url => '/images/logo_taxi_seguro.png'
#)
#Enterprise.create(:name => 'Pontificia Universidad Cat\xC3\xB3lica del Per\xC3\xBA',
#:logo_url => '/images/logo_pucp.gif'
#)
Enterprise.create(:name => 'Pizza Hut',
:logo_url => '/images/logo-pizzahut.gif'
)
Enterprise.create(:name => 'Pinkberry',
:logo_url => '/images/logo-pinkberry.png'
)
Enterprise.create(:name => 'CRISOL',
:logo_url => '/images/logo-crisol.gif'
)
Enterprise.create(:name => 'Travesuras de Chocolate',
:logo_url => '/images/TCH.jpg'
)
Enterprise.create(:name => 'The Z Pillow',
:logo_url => '/images/thezpillow.jpg'
)

Product.delete_all
Product.create(:title => 'Web Design for Developers',
  :description => 
    %{<p>
        <em>Web Design for Developers</em> will show you how to make your
        web-based application look professionally designed. We'll help you
        learn how to pick the right colors and fonts, avoid costly interface
        and accessibility mistakes -- your application will really come alive.
        We'll also walk you through some common Photoshop and CSS techniques
        and work through a web site redesign, taking a new design from concept
        all the way to implementation.
      </p>},
  :image_url =>   '/images/wd4d.jpg',    
  :price => 42.95,
  :enterprise_id => Enterprise.find_by_name('CRISOL').id
)
Product.create(:title => 'Rails Test Prescriptions',
  :description =>
    %{<p>
        <em>Rails Test Prescriptions</em> is a comprehensive guide to testing
        Rails applications, covering Test-Driven Development from both a
        theoretical perspective (why to test) and from a practical perspective
        (how to test effectively). It covers the core Rails testing tools and
        procedures for Rails 2 and Rails 3, and introduces popular add-ons,
        including Cucumber, Shoulda, Machinist, Mocha, and Rcov.
      </p>},
  :image_url => '/images/rtp.jpg',
  :price => 43.75,
  :enterprise_id => Enterprise.find_by_name('CRISOL').id
)
# . . .
Product.create(:title => 'Brewed Coffee',
  :description =>
    %{<p>
        <em>Brewed Coffee</em> One of our rich, flavorful brewed coffees, selected to showcase our roasting and blending artistry.
      </p>},
  :image_url => '/images/brewedcoffee.jpg',
  :price => 10.50,
  :enterprise_id => Enterprise.find_by_name('Starbucks Coffee').id
)
# . . .
Product.create(:title => 'Caffe Misto',
  :description =>
    %{<p>
        <em>Caffe Misto</em> Brewed coffee and steamed milk.
      </p>},
  :image_url => '/images/caffemisto.jpg',
  :price => 8.75,
  :enterprise_id => Enterprise.find_by_name('Starbucks Coffee').id
)
Product.create(:title => 'Coffee Traveler',
  :description =>
    %{<p>
        <em>Coffee Traveler</em> A convenient carrier filled with 96 fl. oz. of brewed coffee.
      </p>},
  :image_url => '/images/coffeetraveler.jpg',
  :price => 17.50,
  :enterprise_id => Enterprise.find_by_name('Starbucks Coffee').id
)
Product.create(:title => 'Double Chocolate Chip',
  :description =>
    %{<p>
        <em>Double Chocolate Chip</em> A creamy blend of rich mocha-flavored sauce, chocolaty chips, milk and ice. Topped with sweetened whipped cream and mocha drizzle.
      </p>},
  :image_url => '/images/doublechocolatechip.jpg',
  :price => 15.50,
  :enterprise_id => Enterprise.find_by_name('Starbucks Coffee').id
)
Product.create(:title => 'TGI California Club',
  :description =>
    %{<p>
        <em>California Club</em> Mesquite-smoked turkey breast, crispy bacon, ham and Monterey Jack cheese stacked high on toasted ciabatta bread with tomatoes, romaine lettuce, avocado and mayonnaise.
      </p>},
  :image_url => '/images/TGI-californiaclub.jpg',
  :price => 21.00,
  :enterprise_id => Enterprise.find_by_name('T.G.I. Fridays').id
)
Product.create(:title => 'Peroni 330ml',
  :description =>
    %{<p>
        <em>Peroni 330ml</em> El estilo italiano en una botella

No puedes saborear la vida si te la tomas con prisa. Eso es lo que dicen los italianos. Rel&aacute;jate, ponte c&oacute;modo y saborea La Dolce Vita.

La cerveza italiana se fabrica as&iacute;, sin dejar que el tiempo apremie, despacio, de una forma pura y natural para asegurar el m&aacute;ximo sabor.
      </p>},
  :image_url => '/images/mSKU_Peroni.jpg',
  :price => 15.00,
  :enterprise_id => Enterprise.find_by_name('PRIMAX').id
)

Product.create(:title => 'Arequipena 620ml',
  :description =>
    %{<p>
        <em>Arequipena 620ml</em> Sabor con car&aacute;cter

Cerveza Arequipena es una marca regional emblem&aacute;tica de Arequipa, que representa la tradici&oacute;n y el car&aacute;cter de los arequipenos, ideal para celebrar a su manera: "a la arequipena".
      </p>},
  :image_url => '/images/mSKU_Arequipena.jpg',
  :price => 25.00,
  :enterprise_id => Enterprise.find_by_name('PRIMAX').id
)

Product.create(:title => 'Quara 310ml',
  :description =>
    %{<p>
        <em>Quara </em> Sabor inspirado en ellas

Quara es la primera bebida alcoh&oacute;lica saborizada con frutas c&iacute;tricas, que no es una cerveza, a base de cebada que se fabrica en el Per&uacute; y que apunta a conquistar paladares que buscan nuevas opciones.
      </p>},
  :image_url => '/images/mSKU_Quara.jpg',
  :price => 15.00,
  :enterprise_id => Enterprise.find_by_name('PRIMAX').id
)

Product.create(:title => 'Sublime',
  :description =>
    %{<p>
        <em>Chocolate Sublime</em> Tableta de chocolate con leche con man&iacute;.

Presentaciones:
Sublime Extremo
Tableta x 72 gr.

Sublime Cl&aacute;sico
Tableta x 32 gr.

Sublime Blanco
Tableta x 30 gr.

Sublime Galleta
Tableta x 28 gr.

Sublime Twist
Barra x 16 gr.

Sublime Bomb&oacute;n
Bomb&oacute;n x 8 gr. 
      </p>},
  :image_url => '/images/Sublime.jpg',
  :price => 2.00,
  :enterprise_id => Enterprise.find_by_name('PRIMAX').id
)

Product.create(:title => 'Dolcetto',
  :description =>
    %{<p>
        <em>Dolcetto </em> 
Delicioso helado con la combinaci&oacute;n perfecta de vainilla con l&uacute;cuma, banado en chocolate con man&iacute;.

Presentaci&oacute;n:

Dolcetto L&uacute;cuma
Empaque x 89 ml.
      </p>},
  :image_url => '/images/dolcetto203x314.jpg',
  :price => 4.00,
  :enterprise_id => Enterprise.find_by_name('PRIMAX').id
)
Product.create(:title => 'DC Universe',
  :description => %{DC Universe for PS3},
  :image_url => '/images/dcuniverse_cover.jpg',
  :price => 90,
  :enterprise_id => Enterprise.find_by_name('SAGA falabella').id
)
Product.create(:title => 'Corazones de chocolate',
  :description => %{8 bombones de chocolate grandes y 4 bombones medianos.
Sabores: Mocca, Whisky, Pisco y Menta},
  :image_url => '/images/chocohearts.jpg',
  :price => 30,
  :enterprise_id => Enterprise.find_by_name('Travesuras de Chocolate').id
)
Product.create(:title => 'Pizza Americana',
  :description => %{Jam&oacute;n y queso mozzarella.},
  :image_url => '/images/pizzahut-americana.jpg',
  :price => 27,
  :enterprise_id => Enterprise.find_by_name('Pizza Hut').id
)
Product.create(:title => 'KFC Original recipe (tm)',
  :description => %{<p>It's what made Colonel Sanders and KFC famous. He developed the secret formula of 11 herbs and spices in the 1930's in Corbin, Kentucky, and it still defines the irresistible flavor of KFC today. We bread juicy pieces of chicken in this world-renowned seasoning and lightly fry it to golden perfection.

With such a rich history and defining taste, it's no wonder the Original Recipe is so proudly guarded.</p>},
  :image_url => '/images/kfc_original_recipe.gif',
  :price => 20,
  :enterprise_id => Enterprise.find_by_name('Kentucky Fried Chicken').id
)
Product.create(:title => 'Z Pillow',
  :description => %{The Z Pillow},
  :image_url => '/images/zpillow-1.jpg',
  :price => 20,
  :enterprise_id => Enterprise.find_by_name('The Z Pillow').id
)

Order.delete_all   #### Importante Borrar ORDERS al regenerar USERS/ENTERPRISES/etc.
User.delete_all

User.create(:nickname => "hugo", :password => "secret", :name => "Hugo", :type_id => "1")
User.create(:nickname => "tom", :password => "tomtom2", :name => "Tom")
User.create(:nickname => "jessica", :password => "south", :name => "Jessica")
User.create(:nickname => "starbucks01", :password => "starbucks01", :type_id => "2", 
   :enterprise_id => Enterprise.find_by_name('Starbucks Coffee').id,
   :name => "Starbucks Counter 01",
   :image_url => '/images/logo_Starbucks_Coffee.gif')
User.create(:nickname => "pizzahut01", :password => "pizzahut", :type_id => "2", 
   :enterprise_id => Enterprise.find_by_name('Pizza Hut').id,
   :name => "Pizza Hut Counter 01",
   :image_url => '/images/logo-pizzahut.gif')

Betatester.create(:email => 'jpablo@gmail.com')
Betatester.create(:email => 'gshsy@yahoo.com')
Betatester.create(:email => 'evivasp@gmail.com')
Betatester.create(:email => 'mariana.isabel@gmail.com')
Betatester.create(:email => 'maracaja.coutinho@gmail.com')
Betatester.create(:email => 'marcela@slapp.com.br')
